package com.example.a20220426_vanessagonzalez_nycschools.apis

import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import retrofit2.http.GET

interface SchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun fetchHighSchools(): List<HighSchool>
}