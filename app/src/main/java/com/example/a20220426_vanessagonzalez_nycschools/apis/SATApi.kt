package com.example.a20220426_vanessagonzalez_nycschools.apis

import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchoolSATResults
import retrofit2.http.GET
import retrofit2.http.Query

interface SATApi {

    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSATScore(
        @Query("dbn") districtBoroughNumber: String
    ): List<HighSchoolSATResults>
}