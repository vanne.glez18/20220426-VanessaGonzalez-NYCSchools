package com.example.a20220426_vanessagonzalez_nycschools.model

sealed class NetworkResponse<out T : Any> {
    object Failure : NetworkResponse<Nothing>()
    data class Success<out T : Any>(
        val data: T
    ) : NetworkResponse<T>()
}
