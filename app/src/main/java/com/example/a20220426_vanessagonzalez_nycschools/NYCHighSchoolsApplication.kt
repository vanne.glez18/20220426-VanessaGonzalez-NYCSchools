package com.example.a20220426_vanessagonzalez_nycschools

import com.example.a20220426_vanessagonzalez_nycschools.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class NYCHighSchoolsApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}