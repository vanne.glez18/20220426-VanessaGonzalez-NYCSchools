package com.example.a20220426_vanessagonzalez_nycschools.repository

import com.example.a20220426_vanessagonzalez_nycschools.apis.SATApi
import com.example.a20220426_vanessagonzalez_nycschools.apis.SchoolsApi
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchoolSATResults
import com.example.a20220426_vanessagonzalez_nycschools.model.NetworkResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface HighSchoolsRepositoryContract {

    suspend fun fetchNYCHighSchools(): NetworkResponse<List<HighSchool>>
    suspend fun fetchSATResults(dbn: String): NetworkResponse<List<HighSchoolSATResults>>
    fun cacheHighSchool(highSchool: HighSchool)
    fun getCachedHighSchool(): HighSchool?
}

class HighSchoolsRepository(
    private val schoolsApi: SchoolsApi,
    private val satApi: SATApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : HighSchoolsRepositoryContract {

    private var cachedHighSchool: HighSchool? = null

    override suspend fun fetchNYCHighSchools() = withContext(dispatcher) {
        try {
            NetworkResponse.Success(schoolsApi.fetchHighSchools())
        } catch (e: Throwable) {
            NetworkResponse.Failure
        }
    }

    override suspend fun fetchSATResults(districtBoroughNumber: String) = withContext(dispatcher) {
        return@withContext try {
            NetworkResponse.Success(satApi.fetchSATScore(districtBoroughNumber))
        } catch (e: Throwable) {
            NetworkResponse.Failure
        }
    }

    override fun cacheHighSchool(highSchool: HighSchool) {
        cachedHighSchool = highSchool
    }

    override fun getCachedHighSchool() = cachedHighSchool
}