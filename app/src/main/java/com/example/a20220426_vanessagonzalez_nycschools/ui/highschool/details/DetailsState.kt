package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details

import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchoolSATResults

sealed class DetailsState {
    object None : DetailsState()
    object Loading : DetailsState()
    object SATFailure : DetailsState()
    object Failure : DetailsState()
    data class SATSuccess(
        val satData: HighSchoolSATResults
    ) : DetailsState()

    data class HighSchoolSuccess(
        val highSchool: HighSchool
    ) : DetailsState()
}