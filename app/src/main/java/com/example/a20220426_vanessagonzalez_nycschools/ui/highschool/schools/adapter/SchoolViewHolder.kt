package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220426_vanessagonzalez_nycschools.databinding.RowItemHighschoolBinding
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool

interface SchoolViewHolderContract {

    fun bindModelToView(model: HighSchool)
}

class SchoolViewHolder(
    parent: ViewGroup,
    val schoolSelectedListener: SchoolSelectedListener,
    val binding: RowItemHighschoolBinding = RowItemHighschoolBinding.inflate(
        LayoutInflater.from(
            parent.context
        ), parent, false
    )
) : RecyclerView.ViewHolder(binding.root), SchoolViewHolderContract {
    override fun bindModelToView(model: HighSchool) {
        binding.highschoolName.text = model.school_name
        binding.root.setOnClickListener {
            schoolSelectedListener.onHighSchoolClicked(model)
        }
    }
}