package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools

import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool

sealed class SchoolsState {
    object None : SchoolsState()
    object Loading : SchoolsState()
    object Failure : SchoolsState()
    data class Success(
        val highSchools: List<HighSchool>
    ) : SchoolsState()
}
