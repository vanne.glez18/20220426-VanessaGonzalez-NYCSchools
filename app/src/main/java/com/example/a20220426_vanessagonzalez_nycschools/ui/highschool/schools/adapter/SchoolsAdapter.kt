package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool

class SchoolsAdapter(val schoolSelectedListener: SchoolSelectedListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val highSchools = mutableListOf<HighSchool>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SchoolViewHolder(parent, schoolSelectedListener)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SchoolViewHolderContract).bindModelToView(highSchools[position])
    }

    override fun getItemCount() = highSchools.size

    fun addHighSchools(newHighSchools: List<HighSchool>) {
        highSchools.addAll(newHighSchools)
        notifyItemRangeInserted(newHighSchools.size, highSchools.size - newHighSchools.size)
    }
}