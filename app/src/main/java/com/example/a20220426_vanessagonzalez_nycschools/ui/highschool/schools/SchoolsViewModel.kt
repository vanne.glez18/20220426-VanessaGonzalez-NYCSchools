package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.model.NetworkResponse
import com.example.a20220426_vanessagonzalez_nycschools.repository.HighSchoolsRepositoryContract
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolsViewModel @Inject constructor(
    internal val highSchoolsRepository: HighSchoolsRepositoryContract
) : ViewModel() {

    private val _state: MutableStateFlow<SchoolsState> =
        MutableStateFlow(SchoolsState.None)
    val state: StateFlow<SchoolsState> by lazy { _state }
    private var isLoading = false

    fun fetchHighSchools() {
        if (isLoading) return
        isLoading = true
        _state.value = SchoolsState.Loading
        viewModelScope.launch {
            _state.value = when (val response = highSchoolsRepository.fetchNYCHighSchools()) {
                NetworkResponse.Failure -> SchoolsState.Failure
                is NetworkResponse.Success -> SchoolsState.Success(response.data)
            }
        }
        isLoading = false
    }

    fun storeHighSchool(highSchool: HighSchool) {
        highSchoolsRepository.cacheHighSchool(highSchool)
    }
}