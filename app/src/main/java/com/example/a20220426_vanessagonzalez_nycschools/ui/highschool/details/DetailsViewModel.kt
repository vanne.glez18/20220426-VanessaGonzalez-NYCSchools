package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220426_vanessagonzalez_nycschools.model.NetworkResponse
import com.example.a20220426_vanessagonzalez_nycschools.repository.HighSchoolsRepositoryContract
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    internal var highSchoolsRepository: HighSchoolsRepositoryContract
) : ViewModel() {

    private var isLoading = false
    private val _state: MutableStateFlow<DetailsState> =
        MutableStateFlow(DetailsState.None)
    val state: StateFlow<DetailsState> by lazy { _state }

    fun getCachedHighSchool() {
        _state.value =
            when (val result = highSchoolsRepository.getCachedHighSchool()) {
                null -> DetailsState.Failure
                else -> DetailsState.HighSchoolSuccess(result)
            }
    }

    fun getSATScores(districtBoroughNumber: String?) {
        if (districtBoroughNumber == null) {
            _state.value = DetailsState.SATFailure
            return
        }
        if (isLoading) return
        isLoading = true
        _state.value = DetailsState.Loading
        viewModelScope.launch {
            val result = highSchoolsRepository.fetchSATResults(districtBoroughNumber)
            _state.value =
                when {
                    result is NetworkResponse.Success && result.data.isNotEmpty() -> DetailsState.SATSuccess(
                        result.data[0]
                    )
                    else -> DetailsState.SATFailure
                }
        }
    }
}