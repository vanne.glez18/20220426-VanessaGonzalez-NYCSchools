package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.adapter

import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool

interface SchoolSelectedListener {

    fun onHighSchoolClicked(highSchool: HighSchool)
}