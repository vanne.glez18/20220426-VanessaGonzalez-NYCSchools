package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.a20220426_vanessagonzalez_nycschools.R
import com.example.a20220426_vanessagonzalez_nycschools.databinding.FragmentDetailsBinding
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsFragment : DaggerFragment() {

    private lateinit var binding: FragmentDetailsBinding

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private val detailsViewModel: DetailsViewModel by lazy {
        ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[DetailsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        detailsViewModel.getCachedHighSchool()
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            detailsViewModel.state.collectLatest { state ->
                when (state) {
                    DetailsState.Failure -> toastError(getString(R.string.no_highschool_found_message))
                    is DetailsState.HighSchoolSuccess -> handleHighSchoolSuccess(state)
                    DetailsState.SATFailure -> toastError(getString(R.string.no_sat_found_message))
                    is DetailsState.SATSuccess -> handleSATSuccess(state)
                }
            }
        }
    }

    private fun toastError(errorMessage: String) {
        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun handleSATSuccess(state: DetailsState.SATSuccess) {
        if (state.satData.sat_math_avg_score != null) binding.mathSatScore.text =
            getString(R.string.avg_math_score, state.satData.sat_math_avg_score)
        if (state.satData.sat_critical_reading_avg_score != null) binding.readingSatScore.text =
            getString(R.string.avg_reading_score, state.satData.sat_critical_reading_avg_score)
        if (state.satData.sat_writing_avg_score != null) binding.writingSatScore.text =
            getString(R.string.avg_writing_score, state.satData.sat_writing_avg_score)
    }

    private fun handleHighSchoolSuccess(state: DetailsState.HighSchoolSuccess) {
        populateView(state.highSchool)
        detailsViewModel.getSATScores(state.highSchool.districtBoroughNumber)
    }

    private fun populateView(highSchool: HighSchool) {
        binding.highschoolName.text = highSchool.school_name
        binding.highschoolDetails.text = highSchool.overview_paragraph
    }
}