package com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.a20220426_vanessagonzalez_nycschools.MainActivity
import com.example.a20220426_vanessagonzalez_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.adapter.SchoolSelectedListener
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.adapter.SchoolsAdapter
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolsFragment : DaggerFragment(), SchoolSelectedListener {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private val schoolsViewModel: SchoolsViewModel by lazy {
        ViewModelProvider(
            requireActivity(),
            viewModelFactory
        )[SchoolsViewModel::class.java]
    }
    private val highSchoolListingAdapter by lazy { SchoolsAdapter(this) }

    private lateinit var binding: FragmentSchoolsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        observeViewModel()
        fetchHighSchoolListing()
    }

    private fun setupRecyclerView() {
        binding.highschoolRecycler.adapter = highSchoolListingAdapter
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            schoolsViewModel.state.collectLatest { state ->
                when (state) {
                    SchoolsState.Failure -> showFailureState()
                    SchoolsState.Loading -> showLoadingState()
                    is SchoolsState.Success -> showSuccessState(state)
                }
            }
        }
    }

    private fun showSuccessState(state: SchoolsState.Success) {
        highSchoolListingAdapter.addHighSchools(state.highSchools)
        binding.highschoolRecycler.visibility = View.VISIBLE
        binding.errorMessage.visibility = View.GONE
        binding.progressBar.visibility = View.GONE
    }

    private fun showFailureState() {
        binding.progressBar.visibility = View.GONE
        binding.errorMessage.visibility = View.VISIBLE
        binding.highschoolRecycler.visibility = View.GONE
    }

    private fun showLoadingState() {
        binding.progressBar.visibility = View.VISIBLE
        binding.errorMessage.visibility = View.GONE
        binding.highschoolRecycler.visibility = View.GONE
    }

    private fun fetchHighSchoolListing() {
        if (schoolsViewModel.state.value !is SchoolsState.Success)
            schoolsViewModel.fetchHighSchools()
    }

    override fun onHighSchoolClicked(highSchool: HighSchool) {
        schoolsViewModel.storeHighSchool(highSchool)
        try {
            (activity as MainActivity).navigateToHighSchoolDetails()
        } catch (e: Throwable) {
        }
    }
}