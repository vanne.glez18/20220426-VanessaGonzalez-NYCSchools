package com.example.a20220426_vanessagonzalez_nycschools.di.modules

import com.example.a20220426_vanessagonzalez_nycschools.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = [
            FragmentBuilderModule::class,
            ViewModelFactoryModule::class,
        ]
    )

    abstract fun contributeMainActivity(): MainActivity
}