package com.example.a20220426_vanessagonzalez_nycschools.di.modules

import androidx.lifecycle.ViewModelProvider
import com.example.a20220426_vanessagonzalez_nycschools.di.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactoryModule(factory: ViewModelFactory): ViewModelProvider.Factory
}