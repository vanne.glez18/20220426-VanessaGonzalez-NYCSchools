package com.example.a20220426_vanessagonzalez_nycschools.di.modules

import com.example.a20220426_vanessagonzalez_nycschools.apis.SATApi
import com.example.a20220426_vanessagonzalez_nycschools.apis.SchoolsApi
import com.example.a20220426_vanessagonzalez_nycschools.repository.HighSchoolsRepository
import com.example.a20220426_vanessagonzalez_nycschools.repository.HighSchoolsRepositoryContract
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideHighSchoolsRepository(
        schoolsApi: SchoolsApi,
        satApi: SATApi
    ): HighSchoolsRepositoryContract =
        HighSchoolsRepository(schoolsApi, satApi)
}