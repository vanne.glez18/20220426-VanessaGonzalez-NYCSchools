package com.example.a20220426_vanessagonzalez_nycschools.di.modules

import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details.DetailsFragment
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.SchoolsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun provideHighSchoolListingFragment(): SchoolsFragment

    @ContributesAndroidInjector
    abstract fun provideHighSchoolDetailsFragment(): DetailsFragment
}