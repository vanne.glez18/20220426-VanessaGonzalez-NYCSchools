package com.example.a20220426_vanessagonzalez_nycschools.di.modules

import androidx.lifecycle.ViewModel
import com.example.a20220426_vanessagonzalez_nycschools.di.viewmodel.ViewModelKey
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details.DetailsViewModel
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.SchoolsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolsViewModel::class)
    abstract fun bindHighSchoolListingViewModel(viewModel: SchoolsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindHighSchoolDetailsViewModel(viewModel: DetailsViewModel): ViewModel
}