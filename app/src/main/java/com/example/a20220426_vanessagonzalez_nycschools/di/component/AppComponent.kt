package com.example.a20220426_vanessagonzalez_nycschools.di.component

import android.app.Application
import com.example.a20220426_vanessagonzalez_nycschools.NYCHighSchoolsApplication
import com.example.a20220426_vanessagonzalez_nycschools.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        ViewModelFactoryModule::class,
        NetworkModule::class,
        RepositoryModule::class
    ]
)

interface AppComponent : AndroidInjector<NYCHighSchoolsApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }
}