package com.example.a20220426_vanessagonzalez_nycschools

import android.os.Bundle
import com.example.a20220426_vanessagonzalez_nycschools.databinding.ActivityMainBinding
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details.DetailsFragment
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.schools.SchoolsFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navigateToHighListing()
    }

    fun navigateToHighSchoolDetails() {
        supportFragmentManager.beginTransaction().addToBackStack(DETAILS_FRAGMENT)
            .replace(binding.fragmentContainer.id, DetailsFragment())
            .commit()
    }

    private fun navigateToHighListing() {
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, SchoolsFragment(), LIST_FRAGMENT)
            .commit()
    }

    companion object {
        val DETAILS_FRAGMENT = "detailsFragment"
        val LIST_FRAGMENT = "listFragment"
    }
}