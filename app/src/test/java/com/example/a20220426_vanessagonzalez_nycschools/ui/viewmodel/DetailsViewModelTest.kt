package com.example.a20220426_vanessagonzalez_nycschools.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchoolSATResults
import com.example.a20220426_vanessagonzalez_nycschools.model.NetworkResponse
import com.example.a20220426_vanessagonzalez_nycschools.repository.HighSchoolsRepositoryContract
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details.DetailsState
import com.example.a20220426_vanessagonzalez_nycschools.ui.highschool.details.DetailsViewModel
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailsViewModelTest {

    @get:Rule
    val instantTask: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var repo: HighSchoolsRepositoryContract

    @MockK
    private lateinit var mockSchool: HighSchool

    @MockK
    private lateinit var mockSATResults: HighSchoolSATResults
    private lateinit var viewModel: DetailsViewModel
    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = DetailsViewModel(repo)
    }

    @Test
    fun getCachedHighSchool_testStateUpdated() {
        // Given
        every { repo.getCachedHighSchool() } returns mockSchool

        // When
        viewModel.getCachedHighSchool()

        // Then
        verify { repo.getCachedHighSchool() }
        assertTrue(viewModel.state.value is DetailsState.HighSchoolSuccess)
        assertEquals(
            mockSchool,
            (viewModel.state.value as? DetailsState.HighSchoolSuccess)?.highSchool
        )
    }

    @Test
    fun getSATScores_testNullDistrictBoroughNumberReturnsWithFailure() {
        // When
        viewModel.getSATScores(null)

        // Then
        assertTrue(viewModel.state.value is DetailsState.SATFailure)
    }

    @Test
    fun getSATScores_testThatRepoIsCalled() {
        // Given
        val dbn = "test:dbn"
        val mockedAnswer = NetworkResponse.Success(listOf(mockSATResults))
        coEvery { repo.fetchSATResults(dbn) } returns mockedAnswer

        // When
        viewModel.getSATScores(dbn)

        // Then
        coVerify { repo.fetchSATResults(dbn) }
    }
}