package com.example.a20220426_vanessagonzalez_nycschools.repository

import com.example.a20220426_vanessagonzalez_nycschools.apis.SATApi
import com.example.a20220426_vanessagonzalez_nycschools.apis.SchoolsApi
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchool
import com.example.a20220426_vanessagonzalez_nycschools.model.HighSchoolSATResults
import com.example.a20220426_vanessagonzalez_nycschools.model.NetworkResponse
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class HighSchoolsRepositoryTest {
    lateinit var repository: HighSchoolsRepository
    private val dummyException = IllegalStateException("there was an error")

    @MockK
    lateinit var highSchoolApi: SchoolsApi

    @MockK
    lateinit var satApi: SATApi

    @MockK
    lateinit var fakeHighSchoolResponse: List<HighSchool>

    @MockK
    lateinit var fakeSATResponse: List<HighSchoolSATResults>

    @MockK
    lateinit var fakeHighSchool: HighSchool

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = HighSchoolsRepository(highSchoolApi, satApi)
    }

    @Test
    fun `on api failure, fetching NYCHighSchools returns a failure`() = runBlocking {
        //Given
        coEvery { highSchoolApi.fetchHighSchools() }.throws(dummyException)

        //when
        val response = repository.fetchNYCHighSchools()

        //then
        assert(response is NetworkResponse.Failure)
    }

    @Test
    fun `on api Success, fetching NYCHighSchools returns a Success with the data`() = runBlocking {
        //Given
        coEvery { highSchoolApi.fetchHighSchools() }.returns(fakeHighSchoolResponse)

        //when
        val response = repository.fetchNYCHighSchools()

        //then
        assert(response is NetworkResponse.Success)
        assert((response as NetworkResponse.Success).data == fakeHighSchoolResponse)
    }

    @Test
    fun `on SAT Success, fetching NYCHighSchools returns a Success with the data`() = runBlocking {
        //Given
        val fakeDBN = ""
        coEvery { satApi.fetchSATScore(fakeDBN) }.returns(fakeSATResponse)

        //when
        val response = repository.fetchSATResults(fakeDBN)

        //then
        assert(response is NetworkResponse.Success)
        assert((response as NetworkResponse.Success).data == fakeSATResponse)
    }

    @Test
    fun `on SAT Failure, fetching NYCHighSchools returns a Failure state`() = runBlocking {
        //Given
        val fakeDBN = ""
        coEvery { satApi.fetchSATScore(fakeDBN) }.throws(dummyException)

        //when
        val response = repository.fetchSATResults(fakeDBN)

        //then
        assert(response is NetworkResponse.Failure)
    }

    @Test
    fun `get cached highSchool returns the cached highSchool`() = runBlocking {
        //Given
        repository.cacheHighSchool(fakeHighSchool)

        //when
        val response = repository.getCachedHighSchool()

        //then
        assert(response == fakeHighSchool)
    }
}